ttymidi-lite is based on [mod-ttymidi](https://github.com/moddevices/mod-ttymidi), using a more simplistic approach, and **without** JACK dependency. Instead, it uses ALSA rawmidi API, and send each byte received on the serial port to the device to the virtual MIDI device.

## COMPILATION

The ttyMIDI source code is comprised of a single C file.  To compile it, just
run the following command:

    make

This program depends on ALSA, so you should have the development headers
for that installed. In Debian or Ubuntu, you can install it by running:

    apt-get install libasound2-dev

After you compile ttyMIDI, you may wish to copy it to /usr/bin for easy
access. This can be done simply by following command:

    sudo make install

## USAGE

First, you need an external device that can send MIDI commands through the
serial port.  To find out more about programming an external device, read the
TTYMIDI SPECIFICATION section.  If you are using an Arduino board
(http://www.arduino.cc), read the instructions under the arduino folder.  Once
your device is programmed and connected to your PC's serial port, follow the
instructions below.  

To connect to ttyS0 at 2400bps:

    ttymidi -s /dev/ttyS0 -b 2400

To connect to ttyUSB port at default speed (31250 bps) and display information
about incoming MIDI events: 

    ttymidi -s /dev/ttyUSB0 -v

## Missing features

- Midi out
- Latency test

## Tested serial interfaces for 31250 bps

Not every serial interface are able to work at MIDI's native baudrate. Here is a list of the interfaces that have been tested working (or not working) out of the box.

| Interface                                                    | Working status                        |
| ------------------------------------------------------------ | ------------------------------------- |
| QinHeng Electronics CH340 serial converter ( USB 1a86:7523 ) | OK                                    |
| Silicon Labs CP210x UART Bridge (USB 10c4:ea60)              | Nok (takes closest standard baudrate) |
| AM335x native UART                                           | OK                                    |
