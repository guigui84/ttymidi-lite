/*
    This file is part of ttymidi.

    ttymidi is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ttymidi is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ttymidi.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <argp.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <alsa/asoundlib.h>

#include <linux/serial.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <asm/termios.h>
#include <assert.h>
// MOD specific
#include "mod-semaphore.h"

#define MAX_DEV_STR_LEN               32
#define MAX_MSG_SIZE                1024
#define DEBUG 1
/* import ioctl definition here, as we can't include both "sys/ioctl.h" and "asm/termios.h" */
extern int ioctl (int __fd, unsigned long int __request, ...) __THROW;

/* --------------------------------------------------------------------- */
// Globals

volatile bool run;
int serial;

/* --------------------------------------------------------------------- */
// Program options

static struct argp_option options[] =
{
	{"serialdevice" , 's', "DEV" , 0, "Serial device to use. Default = /dev/ttyUSB0", 0 },
	{"baudrate"     , 'b', "BAUD", 0, "Serial port baud rate. Default = 31250", 0 },
#ifdef DEBUG
	{"verbose"      , 'v', 0     , 0, "For debugging: Produce verbose output", 0 },
	{"printonly"    , 'p', 0     , 0, "Super debugging: Print values read from serial -- and do nothing else", 0 },
#endif
	{"name"		, 'n', "NAME", 0, "Name of the port client. Default = virtual", 0 },
	{ 0 }
};

typedef struct _arguments
{
#ifdef DEBUG
	int verbose;
	int printonly;
#endif
	char serialdevice[MAX_DEV_STR_LEN];
	int  baudrate;
	char name[MAX_DEV_STR_LEN];
} arguments_t;

void exit_cli(int sig)
{
        run = false;
        printf("\rttymidi closing down ... ");

        // unused
        return; (void)sig;
}

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	   know is a pointer to our arguments structure. */
	arguments_t *arguments = state->input;
	int baud_temp;

	switch (key)
	{
#ifdef DEBUG
		case 'p':
			arguments->printonly = 1;
			break;
		case 'v':
			arguments->verbose = 1;
			break;
#endif
		case 's':
			if (arg == NULL) break;
			strncpy(arguments->serialdevice, arg, MAX_DEV_STR_LEN-1);
			break;
		case 'n':
			if (arg == NULL) break;
			strncpy(arguments->name, arg, MAX_DEV_STR_LEN-1);
			break;
		case 'b':
			if (arg == NULL) break;
			errno = 0;
			baud_temp = strtol(arg, NULL, 0);
			if (errno == EINVAL || errno == ERANGE)
			{
				printf("Baud rate %s is invalid.\n",arg);
				exit(1);
			}
			arguments->baudrate = baud_temp;
			break;
		case ARGP_KEY_ARG:
		case ARGP_KEY_END:
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void arg_set_defaults(arguments_t *arguments)
{
#ifdef DEBUG
	arguments->verbose   = 0;
	arguments->printonly = 0;
#endif
	arguments->baudrate  = 31250;
	strncpy(arguments->serialdevice, "/dev/ttyUSB0", MAX_DEV_STR_LEN);
	strncpy(arguments->name, "virtual", MAX_DEV_STR_LEN);
}

const char *argp_program_version     = "ttymidi 1.0.0";
const char *argp_program_bug_address = "falktx@moddevices.com";
static char doc[]       = "ttymidi - Connect serial port devices to JACK MIDI programs!";
static struct argp argp = { options, parse_opt, NULL, doc, NULL, NULL, NULL };
static arguments_t arguments;

/* --------------------------------------------------------------------- */
// The following read/write wrappers handle the case of interruption by system signals

static inline uint8_t read_retry_or_error(int fd, void* dst)
{
    int error;
    do {
        error = read(fd, dst, 1);
    } while (error == -1 && errno == EINTR);
    return error;
}

static inline uint8_t write_retry_or_success(int fd, const void* src, uint8_t size)
{
    int error;
    do {
        error = write(fd, src, size);
    } while (error == -1 && errno == EINTR);
    // in case of error, return full size so outer loop can stop
    return error >= 0 ? (uint8_t)error : size;
}


//
/* --------------------------------------------------------------------- */
// ALSA stuff


/* --------------------------------------------------------------------- */
// MIDI stuff
/*
void* write_midi_from_jack(void* ptr)
{
        jackdata_t* jackdata = (jackdata_t*) ptr;

        char bufc[4];
        jack_nframes_t buf_frame, buf_diff, cycle_start;
        uint8_t size, written;

        const jack_nframes_t sample_rate = jack_get_sample_rate(jackdata->client);

         * used for select sleep
         * (compared to usleep which sleeps at *least* x us, select sleeps at *most*)
         
        fd_set fd;
        FD_ZERO(&fd);
        struct timeval tv = { 0, 0 };

        while (run)
        {
                if (sem_timedwait_secs(&jackdata->sem, 1) != 0)
                        continue;

                if (! run) break;

                cycle_start = jackdata->last_frame_time;
                buf_diff = 0;

                while (jack_ringbuffer_read(jackdata->ringbuffer_out, bufc, 4) == 4)
                {
                        if (jack_ringbuffer_read(jackdata->ringbuffer_out, (char*)&buf_frame,
                                                 sizeof(jack_nframes_t)) != sizeof(jack_nframes_t))
                            continue;

                        if (buf_frame > cycle_start)
                        {
                            buf_diff   = buf_frame - cycle_start - buf_diff;
                            tv.tv_usec = (buf_diff * 1000000) / sample_rate;

                            if (tv.tv_usec > 60 && tv.tv_usec < 10000  10 ms )
                            {
                                // assume write takes 50 us
                                tv.tv_usec -= 50;
                                select(0, &fd, NULL, NULL, &tv);
                            }
                        }
                        else
                        {
                            buf_diff = 0;
                        }

                        size = (uint8_t)bufc[0];
                        written = 0;

                        do {
                            written += write_retry_or_success(serial, bufc+1+written, size-written);
                        } while (written < size);
                }
        }

        return NULL;
}*/

void* read_midi_from_serial_port(void* ptr)
{
    //jack_midi_data_t buffer[ringbuffer_msg_size];
    unsigned char buffer;
    snd_rawmidi_t* midiout = (snd_rawmidi_t*) ptr;

    if (arguments.printonly)
    {
        while (run) {
            if (read(serial, &buffer, 1) == 1) {
                printf("%02x\t", buffer & 0xFF);
                fflush(stdout);
            }
        }
    }
    else
    {

        ssize_t read_cnt;

        while (run) {
            // Clean the buffer
            buffer=0;

            // Read a byte and go ahead iff it is a valid status byte.
            read_cnt = read(serial, &buffer, 1);
            if (read_cnt != 1) {
                // Nothing to read. Try again in the next loop.
                continue;
            }
            int status;

            if ((status = snd_rawmidi_write(midiout, &buffer, 1)) < 0) {
                printf("Problem writing to MIDI output: %s", snd_strerror(status));
                exit(1);
            }

            #ifdef DEBUG
            if (arguments.verbose) {
                printf("%02x\t", buffer & 0xFF);
                fflush(stdout);
            }
            #endif
            continue;

        }
    }
    fflush(stdout);
    snd_rawmidi_close(midiout);
    return NULL;
}


/* --------------------------------------------------------------------- */
// Main program

static struct termios2 oldtio, newtio;
//static jackdata_t jackdata;
static pthread_t midi_out_thread;

static bool _ttymidi_init(bool exit_on_failure)
{
        int status;
        int mode = SND_RAWMIDI_SYNC;

        snd_rawmidi_t* midiout;// = (snd_rawmidi_t*) client;

        if ((status = snd_rawmidi_open(NULL, &midiout, arguments.name, mode)) < 0) {
            printf("Problem opening MIDI output: %s", snd_strerror(status));
            exit(1);
        }

        serial = open(arguments.serialdevice, O_RDWR | O_NOCTTY);

        if (serial < 0)
        {
                if (exit_on_failure)
                {
                        perror(arguments.serialdevice);
                        exit(-1);
                }
                return false;
        }

        /* save current serial port settings */
        ioctl(serial, TCGETS2, &oldtio);

        /* clear struct for new port settings */
        bzero(&newtio, sizeof(newtio));

        /*
         * CRTSCTS : output hardware flow control (only used if the cable has
         *            all necessary lines. See sect. 7 of Serial-HOWTO)
         * CS8     : 8n1 (8bit, no parity, 1 stopbit)
         * CLOCAL  : local connection, no modem contol
         * CREAD   : enable receiving characters
         */
        newtio.c_cflag = BOTHER | CS8 | CLOCAL | CREAD; // CRTSCTS removed

        /*
         * IGNPAR : ignore bytes with parity errors
         * ICRNL  : map CR to NL (otherwise a CR input on the other computer will not terminate input)
         *           otherwise make device raw (no other input processing)
         */
        newtio.c_iflag = IGNPAR;

        /* Raw output */
        newtio.c_oflag = 0;

        /*
         * ICANON : enable canonical input
         * disable all echo functionality, and don't send signals to calling program
         */
        newtio.c_lflag = 0; // non-canonical

        /* Speed */
        newtio.c_ispeed = arguments.baudrate;
        newtio.c_ospeed = arguments.baudrate;

        /*
         * set up: we'll be reading 4 bytes at a time.
         */
        newtio.c_cc[VTIME] = 0;     /* inter-character timer unused */
        newtio.c_cc[VMIN]  = 1;     /* blocking read until n character arrives */

        /*
         * now activate the settings for the port
         */
        ioctl(serial, TCSETS2, &newtio);

        // Linux-specific: enable low latency mode (FTDI "nagling off")
        struct serial_struct ser_info;
        bzero(&ser_info, sizeof(ser_info));
        ioctl(serial, TIOCGSERIAL, &ser_info);
        ser_info.flags |= ASYNC_LOW_LATENCY;
        ioctl(serial, TIOCSSERIAL, &ser_info);

#ifdef DEBUG
        if (arguments.printonly)
        {
                printf("Super debug mode: Only printing the signal to screen. Nothing else.\n");
        }
#endif

        /*
         * read commands
         */

        run = true;

        /* Give high priority to our threads */
        pthread_attr_t attributes;
        pthread_attr_init(&attributes);
        pthread_attr_setdetachstate(&attributes, PTHREAD_CREATE_JOINABLE);
        pthread_attr_setinheritsched(&attributes, PTHREAD_EXPLICIT_SCHED);
        pthread_attr_setscope(&attributes, PTHREAD_SCOPE_SYSTEM);
        pthread_attr_setschedpolicy(&attributes, SCHED_FIFO);

        struct sched_param rt_param;
        memset(&rt_param, 0, sizeof(rt_param));
        rt_param.sched_priority = 80;

        pthread_attr_setschedparam(&attributes, &rt_param);

        /* Starting thread that is writing jack port data */
        //pthread_create(&midi_out_thread, &attributes, write_midi_from_jack, (void*) &jackdata);

        /* And also thread for polling serial data. As serial is currently read in
           blocking mode, by this we can enable ctrl+c quiting and avoid zombie
           alsa ports when killing app with ctrl+z */
        pthread_t midi_in_thread;
        pthread_create(&midi_in_thread, NULL, read_midi_from_serial_port, (void*) midiout);

        pthread_attr_destroy(&attributes);
        return true;
}

void _ttymidi_finish(void)
{
       // close_client(&jackdata);
        pthread_join(midi_out_thread, NULL);

        /* restore the old port settings */
        ioctl(serial, TCSETS2, &oldtio);
        printf("\ndone!\n");
}

int main(int argc, char** argv)
{
    arg_set_defaults(&arguments);
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (! _ttymidi_init(true))
            return 1;

    signal(SIGINT, exit_cli);
    signal(SIGTERM, exit_cli);

    while (run)
            usleep(100000); // 100 ms

    _ttymidi_finish();
    return 0;
}
